module Main exposing (..)

import Algo exposing (..)
import Browser
import Browser.Dom
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onBlur, onClick, onInput)
import Task


main : Program () Model Msg
main =
    Browser.element
        { init = always ( initialModel, Cmd.none )
        , update = update
        , subscriptions = always Sub.none
        , view = view
        }


type alias Model =
    { records : File
    , queryStr : String
    , underEdit : Maybe EditData
    }


type alias EditData =
    { path : Path, currentValue : String }


initialModel : Model
initialModel =
    { records =
        Directory "animals"
            [ Directory "dogs" [ TextFile "zokni", TextFile "zenith" ]
            , Directory "cats" [ TextFile "helen", TextFile "wagner" ]
            ]
    , queryStr = "animals/all/zokni"
    , underEdit = Nothing
    }


type alias Path =
    List String


type Msg
    = DeleteFile Path
    | AddTextFile Path
    | AddDirectory Path
    | UpdateQuery String
    | Edit EditData
    | DoneEditing EditData
    | Nop


boolToColor : Bool -> String
boolToColor bool =
    if bool then
        "green"

    else
        "red"


view : Model -> Html Msg
view ({ records, queryStr } as model) =
    div [ style "padding" "10px" ]
        [ div [ style "color" "gray", style "padding-bottom" "5px" ] [ text "query text" ]
        , div [ style "display" "flex", style "gap" "5px" ]
            [ input [ value queryStr, onInput UpdateQuery ] []
            , div
                [ style "background-color" (boolToColor (query (parse queryStr) records))
                , style "width" "20px"
                , style "height" "20px"
                , style "border-radius" "10px"
                ]
                []
            ]
        , div [ style "color" "gray", style "padding" "10px 0 5px 0" ] [ text "click to edit filenames" ]
        , viewRecords model
        ]


viewRecords : Model -> Html Msg
viewRecords { records, underEdit } =
    let
        viewRecordsHelper path file =
            let
                level =
                    List.length path

                displayDelete =
                    if level == 0 then
                        "none"

                    else
                        "block"

                displayNew =
                    case file of
                        Directory _ _ ->
                            "flex"

                        _ ->
                            "none"

                ( name, subFiles, prefix ) =
                    case file of
                        Directory dirname files ->
                            ( dirname, files |> List.map (viewRecordsHelper (path ++ [ dirname ])), "📁" )

                        TextFile filename ->
                            ( filename, [], "📝" )
            in
            div
                [ style "position" "relative"
                , style "left" "20px"
                ]
                (div
                    [ style "display" "flex", style "gap" "5px" ]
                    [ viewName prefix underEdit (path ++ [ name ]) name
                    , div [ style "cursor" "pointer", style "display" displayDelete, onClick (DeleteFile (path ++ [ name ])) ] [ text "╳" ]
                    ]
                    :: subFiles
                    ++ [ div [ style "opacity" "0.4", style "cursor" "pointer", style "display" displayNew, style "padding-left" "20px" ]
                            [ div [ onClick (AddDirectory (path ++ [ name ])) ] [ text "+📁" ]
                            , text "/"
                            , div [ onClick (AddTextFile (path ++ [ name ])) ] [ text "+📝" ]
                            ]
                       ]
                )
    in
    div [ style "margin-left" "-20px" ] [ viewRecordsHelper [] records ]


viewName : String -> Maybe EditData -> Path -> String -> Html Msg
viewName prefix underEdit currentPath name =
    let
        default =
            div [ onClick (Edit { path = currentPath, currentValue = name }) ] [ text name ]
    in
    div [ style "display" "flex" ]
        [ div [ onClick (Edit { path = currentPath, currentValue = name }) ] [ text prefix ]
        , case underEdit of
            Just ({ path, currentValue } as editData) ->
                if currentPath == path then
                    input [ id "edit-box", value currentValue, onInput (\str -> Edit { path = path, currentValue = str }), onBlur (DoneEditing editData) ] []

                else
                    default

            _ ->
                default
        ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ({ records } as model) =
    case msg of
        UpdateQuery newQueryStr ->
            ( { model | queryStr = newQueryStr }, Cmd.none )

        DeleteFile path ->
            ( { model | records = delete path records |> Maybe.withDefault records }, Cmd.none )

        Edit editData ->
            ( { model | underEdit = Just editData }, Task.attempt (always Nop) (Browser.Dom.focus "edit-box") )

        DoneEditing { path, currentValue } ->
            ( { model | underEdit = Nothing, records = rename currentValue path records }, Cmd.none )

        AddDirectory path ->
            ( { model | underEdit = Just { path = path ++ [ "new" ], currentValue = "new" }, records = addDirectory path records }, Task.attempt (always Nop) (Browser.Dom.focus "edit-box") )

        AddTextFile path ->
            ( { model | underEdit = Just { path = path ++ [ "new" ], currentValue = "new" }, records = addTextFile path records }, Task.attempt (always Nop) (Browser.Dom.focus "edit-box") )

        _ ->
            ( model, Cmd.none )


modify : ((File -> a) -> List File -> List File) -> (File -> a) -> (File -> a) -> Path -> File -> a
modify recurse match nomatch path file =
    case ( path, file ) of
        ( [ dirnameInPath ], Directory dirname _ ) ->
            if dirname == dirnameInPath then
                match file

            else
                nomatch file

        ( pathHead :: pathTail, Directory dirname files ) ->
            if pathHead == dirname then
                files |> recurse (modify recurse match nomatch pathTail) |> Directory dirname |> nomatch

            else
                nomatch file

        ( [ filenameInPath ], TextFile filename ) ->
            if filename == filenameInPath then
                match file

            else
                nomatch file

        _ ->
            nomatch file


delete : Path -> File -> Maybe File
delete =
    modify List.filterMap (always Nothing) Just


rename : String -> Path -> File -> File
rename newName =
    let
        changeFileName file =
            case file of
                Directory _ files ->
                    Directory newName files

                TextFile _ ->
                    TextFile newName
    in
    modify List.map changeFileName identity


addDirectory : Path -> File -> File
addDirectory =
    let
        addNewDirectory file =
            case file of
                Directory dirname files ->
                    Directory dirname (files ++ [ Directory "new" [] ])

                TextFile _ ->
                    file
    in
    modify List.map addNewDirectory identity


addTextFile : Path -> File -> File
addTextFile =
    let
        addNewDirectory file =
            case file of
                Directory dirname files ->
                    Directory dirname (files ++ [ TextFile "new" ])

                TextFile _ ->
                    file
    in
    modify List.map addNewDirectory identity
