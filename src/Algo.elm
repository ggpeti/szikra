module Algo exposing (..)


query : QueryPath -> File -> Bool
query path currentFile =
    case ( path, currentFile ) of
        ( Node title Terminal, TextFile fileTitle ) ->
            title == fileTitle

        ( Node title rest, Directory dirTitle files ) ->
            title == dirTitle && (files |> List.any (query rest))

        ( WildcardNode Terminal, TextFile _ ) ->
            True

        ( WildcardNode rest, Directory _ files ) ->
            files |> List.any (query rest)

        _ ->
            False


type File
    = Directory String (List File)
    | TextFile String


type QueryPath
    = Terminal
    | Node String QueryPath
    | WildcardNode QueryPath


parseComponent : String -> QueryPath -> QueryPath
parseComponent str =
    case str of
        "all" ->
            WildcardNode

        _ ->
            Node str


parse : String -> QueryPath
parse =
    String.split "/" >> List.foldr parseComponent Terminal
